var request = require('request'),
    fs = require('fs'),
    https = require('https'),
    formData = {
    };
// Adding authentication headers
var headers = {
    'X-ApplicationID': 'd18610af-18d0-4d12-81a5-d9d72fb2ac24',
    'X-SecretKey': '660cacb0-16ef-4a4c-90ad-9471a3331036'
};

// Result of this request is a JSON object which contains information about conversion
// including fileDownloadUrl that is used to get the resulting file
var conversionRequest = request.post({
    url: 'https://api2.docconversionapi.com/jobs/create',
    formData: formData,
    rejectUnauthorized: false,
    headers: headers,
    encoding: 'binary'
}, function (err, response) {
    
    var responseParsed = null;
    if (response.statusCode == 200) {
        // Parsing response JSON object and getting fileDownloadUrl value
        responseParsed = JSON.parse(response.body);
        console.log(responseParsed.fileDownloadUrl);
        // Downloading the file and writing it to the local folder
        const resultFile = fs.createWriteStream('output.docx');
        https.get(responseParsed.fileDownloadUrl, function (response) {
            response.pipe(resultFile);
        });
    }
});

// Adding all parameters to multipart form
var form = conversionRequest.form();
form.append("inputFile", fs.readFileSync('example.pdf'), "example.pdf");
form.append("conversionParameters", "{}");
form.append("async", "false");
form.append("outputFormat", "docx");